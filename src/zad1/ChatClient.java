/**
 * @author Smyrski Jakub S25642
 */

package zad1;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import static zad1.ServerCommunicationProperties.*;

public class ChatClient {
    private final String id;
    private final Queue<String> clientLogs;
    private final SocketChannel socketChannel;

    public ChatClient(String host, int port, String id) throws IOException {
        this.id = id;
        clientLogs = new ConcurrentLinkedQueue<>();
        socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress(host, port));
        socketChannel.configureBlocking(false);
    }

    public void listen(CountDownLatch countDownLatch) {
        new Thread(() -> {
            boolean foundLogout = false;
            while (true) {
                try {
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    int bytesRead = socketChannel.read(buffer);

                    if (bytesRead > 0) {
                        buffer.flip();

                        byte[] bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        String response = new String(bytes);

                        List<String> msgs = Arrays.stream(response.split(MESSAGE_SEPARATOR_POSTFIX))
                                .filter(s -> !s.trim().isEmpty())
                                .collect(Collectors.toList());

                        for (String msg : msgs) {
                            if (msg.equals(String.format(USER_LOGGED_OUT_TEMPLATE, id))) {
                                foundLogout = true;
                            }
                        }

                        clientLogs.addAll(msgs);
                        if (foundLogout) {
                            countDownLatch.countDown();
                            break;
                        }
                    }
                } catch (IOException exc) {
                    clientLogs.add("***" + exc);
                }
            }
        }).start();
    }

    public void login() {
        sendOperationRequest(LOGIN_TEMPLATE);
    }

    public void logout() {
        sendOperationRequest(LOGOUT_TEMPLATE);
    }

    public void send(String req) {
        try {
            byte[] message = String.format(MESSAGE_TEMPLATE, id, req).getBytes(StandardCharsets.UTF_8);
            ByteBuffer buffer = ByteBuffer.wrap(message);
            socketChannel.write(buffer);
            buffer.clear();
        } catch (IOException exc) {
            clientLogs.add("***" + exc);
        }
    }

    public String getChatView() {
        return String.format(CLIENT_LOGS_TEMPLATE, id).concat(
                String.join("\n", clientLogs)
        );
    }

    private void sendOperationRequest(String operation) {
        try {
            byte[] logoutMessage = String.format(operation, id).getBytes(StandardCharsets.UTF_8);
            ByteBuffer buffer = ByteBuffer.wrap(logoutMessage);
            socketChannel.write(buffer);
            buffer.clear();
        } catch (IOException exc) {
            clientLogs.add("***" + exc);
        }
    }
}
