package zad1;

public class ServerCommunicationProperties {
    public static final String LOGIN_PREFIX = "login:";
    public static final String LOGOUT_PREFIX = "logout:";
    public static final String MESSAGE_PREFIX = "msg:";
    public static final String MESSAGE_TEMPLATE = "msg:%s:%s||END||"; //format -> msg:{user}:{message}
    public static final String LOGIN_TEMPLATE = "login:%s||END||";
    public static final String LOGOUT_TEMPLATE = "logout:%s||END||";
    public static final String USER_LOGGED_IN_TEMPLATE = "%s logged in";
    public static final String USER_LOGGED_OUT_TEMPLATE = "%s logged out";
    public static final String MSG_TEMPLATE = "%s: %s||END||";
    public static final String CLIENT_LOGS_TEMPLATE = "\n=== %s chat view \n";
    public static final String MESSAGE_SEPARATOR_POSTFIX = "\\|\\|END\\|\\|";
}
