/**
 * @author Smyrski Jakub S25642
 */

package zad1;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import static zad1.ServerCommunicationProperties.*;

public class ChatServer {

    private static final String LOG_TEMPLATE = "%s %s";

    private final Thread chatServerThread;
    private final Queue<String> logQueue;

    public ChatServer(String host, int port) throws IOException {
        logQueue = new ConcurrentLinkedQueue<>();
        chatServerThread = new Thread(new ChatServerWorker(host, port, logQueue));
    }

    public void startServer() {
        chatServerThread.start();
    }

    public void stopServer() {
        chatServerThread.interrupt();
        System.out.println("\nServer stopped");
    }

    public String getServerLog() {
        return String.join("\n", logQueue);
    }

    private static final class ChatServerWorker implements Runnable {
        private final Set<String> loggedUsers;
        private final Set<SocketChannel> connectedChannels;
        private final Queue<String> logQueue;
        private final ServerSocketChannel serverSocketChannel;
        private final Selector selector;


        public ChatServerWorker(String host, int port, Queue<String> logQueue) throws IOException {
            loggedUsers = new LinkedHashSet<>();
            connectedChannels = new LinkedHashSet<>();
            this.logQueue = logQueue;
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(host, port));
            serverSocketChannel.configureBlocking(false);
            selector = Selector.open();
        }

        @Override
        public void run() {
            try {
                startServer();
                while (!Thread.currentThread().isInterrupted()) {
                    int items = selector.select();

                    if (items == 0) {
                        continue;
                    }

                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

                    while (keyIterator.hasNext()) {
                        SelectionKey key = keyIterator.next();
                        if (key.isAcceptable()) {
                            SocketChannel acceptedClient = serverSocketChannel.accept();
                            if (acceptedClient != null) {
                                acceptedClient.configureBlocking(false);
                                acceptedClient.register(selector, SelectionKey.OP_READ);
                            }
                        }
                        if (key.isReadable()) {
                            readMessage(key);
                        }
                        keyIterator.remove();
                    }
                }
                stopServer();
            } catch (IOException e) {
                System.out.println("*** " + e);
            }
        }

        private void startServer() throws IOException {
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("Server started");
        }

        private void stopServer() throws IOException {
            for (SocketChannel connectedChannel : connectedChannels) {
                connectedChannel.close();
            }
            selector.close();
            serverSocketChannel.close();
        }

        private void readMessage(SelectionKey key) throws IOException {
            SocketChannel socketChannel = (SocketChannel) key.channel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            int result = socketChannel.read(byteBuffer);

            if (result > 0) {
                String message = new String(byteBuffer.array(), StandardCharsets.UTF_8);
                List<String> msgs = Arrays.stream(message.split(MESSAGE_SEPARATOR_POSTFIX))
                                .filter(s -> !s.trim().isEmpty())
                                .collect(Collectors.toList());
                for (String msg : msgs) {
                    interpretMessage(msg, socketChannel);
                }
                byteBuffer.clear();
            }
        }

        private void interpretMessage(String message, SocketChannel socketChannel) throws IOException {
            message = message.trim();
            if (!message.isEmpty()) {
                if (message.startsWith(LOGIN_PREFIX)) {
                    loginUser(message, socketChannel);
                } else if (message.startsWith(LOGOUT_PREFIX)) {
                    logoutUser(message, socketChannel);
                } else if (message.startsWith(MESSAGE_PREFIX)) {
                    logQueue.add(String.format(LOG_TEMPLATE, LocalDateTime.now().toLocalTime(), message.split(MESSAGE_PREFIX)[1].replaceAll(":", ": ")));
                    sendMessageToEveryone(message, false);
                }
            }
        }

        private void loginUser(String message, SocketChannel socketChannel) throws IOException {
            String[] splittedUser = message.split(LOGIN_PREFIX);
            loggedUsers.add(splittedUser[1]);
            connectedChannels.add(socketChannel);
            sendMessageToEveryone(String.format(USER_LOGGED_IN_TEMPLATE, splittedUser[1]), true);
            logQueue.add(String.format(LOG_TEMPLATE, LocalDateTime.now().toLocalTime(), String.format(USER_LOGGED_IN_TEMPLATE, splittedUser[1])));
        }

        private void logoutUser(String message, SocketChannel socketChannel) throws IOException {
            String[] splittedUser = message.split(LOGOUT_PREFIX);
            sendMessageToEveryone(String.format(USER_LOGGED_OUT_TEMPLATE, splittedUser[1]), true);
            loggedUsers.remove(splittedUser[1]);
            connectedChannels.remove(socketChannel);
            logQueue.add(String.format(LOG_TEMPLATE, LocalDateTime.now().toLocalTime(), String.format(USER_LOGGED_OUT_TEMPLATE, splittedUser[1])));
        }

        private void sendMessageToEveryone(String message, boolean server) throws IOException {
            String[] splittedMessage = message.split(":");
            if (splittedMessage.length == 3 && userLogged(splittedMessage[1])) {
                for (SocketChannel socketChannel : connectedChannels) {
                    ByteBuffer byteBuffer = ByteBuffer.wrap(String.format(MSG_TEMPLATE, splittedMessage[1], splittedMessage[2]).getBytes(StandardCharsets.UTF_8));
                    socketChannel.write(byteBuffer);
                    byteBuffer.clear();
                }
            } else if (server) {
                for (SocketChannel socketChannel : connectedChannels) {
                    ByteBuffer byteBuffer = ByteBuffer.wrap((message + "||END||").getBytes(StandardCharsets.UTF_8));
                    socketChannel.write(byteBuffer);
                    byteBuffer.clear();
                }
            }
        }

        private boolean userLogged(String userId) {
            return loggedUsers.contains(userId);
        }
    }
}
