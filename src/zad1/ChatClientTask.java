/**
 *
 *  @author Smyrski Jakub S25642
 *
 */

package zad1;


import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

public class ChatClientTask implements RunnableFuture<Void> {

    private final ChatClient chatClient;
    private final Queue<String> msgs;
    private final long wait;
    private final CountDownLatch conditionLatch;

    public ChatClientTask(ChatClient chatClient, List<String> msgs, long wait) {
        this.msgs = new ConcurrentLinkedQueue<>(msgs);
        this.wait = wait;
        this.conditionLatch = new CountDownLatch(1);
        this.chatClient = chatClient;
    }

    public static ChatClientTask create(ChatClient chatClient, List<String> msgs, long wait) {
        return new ChatClientTask(chatClient, msgs, wait);
    }

    public ChatClient getClient() {
        return chatClient;
    }

    @Override
    public void run() {
        try {
            chatClient.login();
            chatClient.listen(conditionLatch);
            checkIfSleepNeeded();
            for (String msg : msgs) {
                chatClient.send(msg);
                msgs.remove(msg);
                checkIfSleepNeeded();
            }
            chatClient.logout();
            checkIfSleepNeeded();
        } catch (InterruptedException e) {
            System.out.println("*** " + e);
        }
    }

    private void checkIfSleepNeeded() throws InterruptedException {
        if (wait > 0) {
            Thread.sleep(wait);
        }
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return msgs.isEmpty();
    }

    @Override
    public Void get() throws InterruptedException, ExecutionException {
        conditionLatch.await();
        return null;
    }

    @Override
    public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}
